/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     KeyFile Unit Tests
 * @details   Test cases for KeyFile class
 *-
 */

#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <utility>

#include "KeyFile.h"
#include "gtest/gtest.h"

using namespace std;
using namespace bswi::kf;

class GTestKeyValueValid : public ::testing::Test
{
protected:
  GTestKeyValueValid();
  virtual ~GTestKeyValueValid();

  virtual void SetUp();
  virtual void TearDown();

protected:
  unique_ptr<KeyFile> m_keyfile;
};

GTestKeyValueValid::GTestKeyValueValid()
: m_keyfile(make_unique<KeyFile>(KeyFile("assets/key_file_example.conf")))
{
}

GTestKeyValueValid::~GTestKeyValueValid() {}

void GTestKeyValueValid::SetUp() {}

void GTestKeyValueValid::TearDown() {}

TEST_F(GTestKeyValueValid, parse)
{
  int status;

  status = m_keyfile->parseFile();
  m_keyfile->printStdOutput();

  EXPECT_EQ(status, 0);
}

int main(int argc, char **argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
