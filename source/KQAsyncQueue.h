/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra KQAsyncQueue Class
 * @details   KQueue based AsyncQueue implementation class
 *-
 */

#pragma once

#include <mutex>
#include <queue>

#include "IAsyncQueue.h"
#include "KQTypes.h"

namespace bswi::event
{

template <class T>
class KQAsyncQueue : public IAsyncQueue<T>
{
public:
  explicit KQAsyncQueue(const std::string &name,
                        const std::function<bool(const T &data)> &callback,
                        IEventSource::Priority priority = IEventSource::Priority::Normal)
  : IAsyncQueue<T>(name, callback, priority)
  {
    IEventSource::setDispatch([this, callback]() {
      bool status = true;

      while (status && !m_queue.empty()) {
        status = callback(m_queue.front());
        std::scoped_lock lk(m_mutex);
        m_queue.pop();
      }

      return status;
    }); // This is our event source dispatch function

    IEventSource::setFinalize([this]() {
      if (m_keventDescriptor.kq != -1) {
        m_keventDescriptor.ev.flags = EV_DELETE;
        kevent(m_keventDescriptor.kq, &m_keventDescriptor.ev, 1, nullptr, 0, nullptr);
      }
    });

    IEventSource::setTrigger([this]() {
      int status = -1;

      if (m_keventDescriptor.kq != -1) {
        m_keventDescriptor.ev.flags = EV_ADD;
        m_keventDescriptor.ev.fflags = NOTE_TRIGGER;
        status = kevent(m_keventDescriptor.kq, &m_keventDescriptor.ev, 1, nullptr, 0, nullptr);
      }
      return (status >= 0);
    });

    m_keventDescriptor.kq = -1;
    m_keventDescriptor.fd = reinterpret_cast<uintptr_t>(this);
    m_keventDescriptor.enable = true;

    EV_SET(&m_keventDescriptor.ev, m_keventDescriptor.fd, EVFILT_USER, 0, NOTE_FFNOP, 0, nullptr);
    IEventSource::m_descriptor = std::make_any<KEventDescriptor *>(&m_keventDescriptor);
  }

  virtual ~KQAsyncQueue() = default;

  bool push(const T &val) final
  {
    std::scoped_lock lk(m_mutex);
    m_queue.push(val);
    return IEventSource::trigger();
  }

private:
  std::queue<T> m_queue;
  std::mutex m_mutex;
  KEventDescriptor m_keventDescriptor = {};
};

} // namespace bswi::event
