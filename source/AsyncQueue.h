/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra AsyncQueue Class
 * @details   AsyncQueue wrapper class
 *-
 */

#pragma once

#if defined(__linux__)
#include "EPAsyncQueue.h"
#else
#include "KQAsyncQueue.h"
#endif

namespace bswi::event
{

#if defined(__linux__)
template <class T>
class AsyncQueue : public EPAsyncQueue<T>
{
public:
  using EPAsyncQueue<T>::EPAsyncQueue;
};
#else
template <class T>
class AsyncQueue : public KQAsyncQueue<T>
{
public:
  using KQAsyncQueue<T>::KQAsyncQueue;
};
#endif

} // namespace bswi::event
