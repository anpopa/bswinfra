/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra UserEvent Class
 * @details   UserEvent wrapper class
 *-
 */

#pragma once

#if defined(__linux__)
#include "EPUserEvent.h"
#else
#include "KQUserEvent.h"
#endif

namespace bswi::event
{

#if defined(__linux__)
class UserEvent : public EPUserEvent
{
public:
  using EPUserEvent::EPUserEvent;
};
#else
class UserEvent : public KQUserEvent
{
public:
  using KQUserEvent::KQUserEvent;
};
#endif

} // namespace bswi::event
