/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra Logger Class
 * @details   Logger class with build time backend selection
 *-
 */

#pragma once

#include <sstream>
#include <string>

namespace bswi::log
{

class Logger
{
public:
  static Logger *getInstance() noexcept { return (!instance) ? instance = new Logger : instance; }

  struct Message {
    enum class Type { Verbose, Debug, Info, Notice, Warn, Error, Fatal };

    explicit Message(Type type)
    : m_type(type){};

    ~Message() { print(); }

    template <class T>
    Message &operator<<(const T &val)
    {
      m_stream << val;
      return *this;
    }

    Message &operator<<(const char *val)
    {
      m_stream << val;
      return *this;
    }

    Message &operator<<(char *val) { return operator<<(const_cast<const char *>(val)); }

    void print();

  private:
    const Type m_type{Type::Info};
    std::stringstream m_stream;
    friend class Logger;
  };

  static Message verbose() { return Message{Message::Type::Verbose}; }

  static Message debug() { return Message{Message::Type::Debug}; }

  static Message info() { return Message{Message::Type::Info}; }

  static Message notice() { return Message{Message::Type::Notice}; }

  static Message warn() { return Message{Message::Type::Warn}; }

  static Message error() { return Message{Message::Type::Error}; }

  static Message fatal() { return Message{Message::Type::Fatal}; }

  static void registerApplication(const std::string &appId, const std::string &description);
  static void setLogLevel(const Message::Type level) { m_level = level; };
  static inline auto getLogLevel(void) -> Message::Type { return m_level; };

public:
  Logger(Logger const &) = delete;
  void operator=(Logger const &) = delete;

private:
  Logger() noexcept = default;
  ~Logger() = default;

private:
  static Logger *instance;
  static std::string m_appId;
  static Message::Type m_level;
};
} // namespace bswi::log

#define logVerbose() ::bswi::log::Logger::verbose()
#define logDebug() ::bswi::log::Logger::debug()
#define logInfo() ::bswi::log::Logger::info()
#define logNotice() ::bswi::log::Logger::notice()
#define logWarn() ::bswi::log::Logger::warn()
#define logError() ::bswi::log::Logger::error()
#define logFatal() ::bswi::log::Logger::fatal()
