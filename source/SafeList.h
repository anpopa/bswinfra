/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra SafeList Class
 * @details   Safe list implementation
 *-
 */

#pragma once

#include <algorithm>
#include <functional>
#include <mutex>
#include <set>
#include <string>
#include <vector>

namespace bswi::util
{

template <class T>
class SafeList
{
public:
  explicit SafeList(const std::string &name)
  : m_name(name){};

public:
  void append(const T &entry, bool sync = false)
  {
    std::scoped_lock lk(m_mutex);
    if (sync) {
      auto it = find(m_queue.begin(), m_queue.end(), entry);
      if (it == m_queue.end()) {
        m_queue.emplace_back(entry);
      }
    } else {
      m_add.insert(entry);
    }
  }
  void remove(const T &entry, bool sync = false)
  {
    std::scoped_lock lk(m_mutex);
    if (sync) {
      m_queue.erase(std::remove(m_queue.begin(), m_queue.end(), entry), m_queue.end());
      auto it = find(m_add.begin(), m_add.end(), entry);
      if (it != m_add.end()) {
        m_add.erase(it);
      }
    } else {
      m_rem.insert(entry);
    }
  }
  void commit()
  {
    std::scoped_lock lk(m_mutex);
    if (m_add.size() > 0) {
      for (const T &entry : m_add) {
        auto it = find(m_queue.begin(), m_queue.end(), entry);
        if (it == m_queue.end()) {
          m_queue.emplace_back(entry);
        }
      }
      m_add.clear();
    }
    if (m_rem.size() > 0) {
      for (const T &entry : m_rem) {
        m_queue.erase(std::remove(m_queue.begin(), m_queue.end(), entry), m_queue.end());
      }
      m_rem.clear();
    }
  }
  void foreach (const std::function<void(const T &)> &cb)
  {
    for (const T &entry : m_queue) {
      cb(entry);
    }
  }
  auto getName() -> const std::string & { return m_name; }
  auto getSize() -> size_t { return m_queue.size(); }

private:
  std::string m_name;
  std::set<T> m_add;
  std::set<T> m_rem;
  std::vector<T> m_queue;
  std::mutex m_mutex;
};

} // namespace bswi::util
