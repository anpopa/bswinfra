/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra Exceptions Classes
 * @details   Exceptions helper classes
 *-
 */

#pragma once

#include <exception>

namespace bswi::except
{

class SingleInstance : public std::exception
{
  [[nodiscard]] const char *what() const noexcept override { return "Object is a singletone"; }
};

class CreateDescriptor : public std::exception
{
  [[nodiscard]] const char *what() const noexcept override { return "Fail to create descriptor"; }
};

class ComputationError : public std::exception
{
  [[nodiscard]] const char *what() const noexcept override { return "Computation result invalid"; }
};

} // namespace bswi::except
