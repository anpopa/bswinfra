/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra KQPathEvent Class
 * @details   KQueue based PathEvent implementation
 *-
 */

#pragma once

#include <map>
#include <mutex>
#include <string>

#include "IPathEvent.h"
#include "KQTypes.h"

namespace bswi::event
{

class KQPathEvent : public IPathEvent
{
public:
  explicit KQPathEvent(
      const std::string &name,
      const std::function<bool(const std::string &path, unsigned eventMask)> &callback,
      unsigned flags,
      Priority priority = Priority::Normal);
  virtual ~KQPathEvent();

  auto addWatch(const std::string &path, unsigned eventMask) -> int final;
  auto remWatch(const std::string &path) -> int final;

private:
  static const size_t m_max_events = 1024;
  struct kevent m_events[m_max_events] {
  };
  std::mutex m_watchesMutex;
  std::map<std::string, int> m_watches;
  unsigned m_flags;
  KEventDescriptor m_keventDescriptor = {};
};

} // namespace bswi::event
