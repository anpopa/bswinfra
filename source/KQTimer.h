/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra KQTimer Class
 * @details   KQueue based Timer implementation
 *-
 */

#pragma once

#include "ITimer.h"
#include "KQTypes.h"

namespace bswi::event
{

class KQTimer : public ITimer
{
public:
  explicit KQTimer(const std::string &name,
                   const std::function<bool()> &callback,
                   Priority priority = Priority::Normal);
  virtual ~KQTimer();

  auto start(uint64_t usec, bool repeat) -> int final;
  auto stop() -> int final;

private:
  KEventDescriptor m_keventDescriptor = {};
};

} // namespace bswi::event
