/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra IPathEvent Class
 * @details   PathEvent interface class
 *-
 */

#pragma once

#include <atomic>

#include "IEventSource.h"

namespace bswi::event
{

class IPathEvent : public IEventSource
{
public:
  typedef struct Events {
    static const unsigned Access = 0x00000001;
    static const unsigned Attrib = 0x00000002;
    static const unsigned Close = 0x00000004;
    static const unsigned CloseWrite = 0x00000008;
    static const unsigned CloseNoWrite = 0x00000010;
    static const unsigned Create = 0x00000020;
    static const unsigned Delete = 0x00000040;
    static const unsigned DeleteSelf = 0x00000080;
    static const unsigned DontFollow = 0x00000100;
    static const unsigned ExclUnlink = 0x00000200;
    static const unsigned MaskAdd = 0x00000400;
    static const unsigned Modify = 0x00000800;
    static const unsigned Move = 0x00001000;
    static const unsigned MoveSelf = 0x00002000;
    static const unsigned MovedFrom = 0x00004000;
    static const unsigned MovedTo = 0x00008000;
    static const unsigned OneShot = 0x00010000;
    static const unsigned OnlyDir = 0x00020000;
    static const unsigned Open = 0x00040000;
  } Events;

  typedef struct Flags {
    static const unsigned NonBlock = 0x00000001;
    static const unsigned CloseExec = 0x00000002;
  } Flags;

  explicit IPathEvent(const std::string &name,
                      const std::function<bool(const std::string &path, unsigned)> & /* callback */,
                      unsigned /* flags */,
                      Priority priority = Priority::Normal)
  {
    m_name = name;
    m_priority = priority;
    setPrepare([]() { return true; });
    setCheck([](int) { return true; });
  }

  virtual auto addWatch(const std::string &path, unsigned eventMask) -> int = 0;
  virtual auto remWatch(const std::string &path) -> int = 0;
};

} // namespace bswi::event
