/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra PathEvent Class
 * @details   PathEvent wrapper class
 *-
 */

#pragma once

#if defined(__linux__)
#include "EPInotify.h"
#else
#include "KQPathEvent.h"
#endif

namespace bswi::event
{

#if defined(__linux__)
class PathEvent : public EPInotify
{
public:
  using EPInotify::EPInotify;
};
#else
class PathEvent : public KQPathEvent
{
public:
  using KQPathEvent::KQPathEvent;
};
#endif

} // namespace bswi::event
