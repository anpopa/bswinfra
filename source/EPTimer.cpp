/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra EPTimer Class
 * @details   EPoll based Timer implementation
 *-
 */

#include "EPTimer.h"
#include "Exceptions.h"

#include <cstdlib>
#include <ctime>
#include <sys/timerfd.h>
#include <unistd.h>

using std::function;
using std::string;

namespace bswi::event
{

EPTimer::EPTimer(const string &name, const function<bool()> &callback, Priority priority)
: ITimer(name, callback, priority)
{
  setDispatch([this, callback]() {
    uint64_t exp_cnt = 0;
    bool status = false;
    ssize_t sz;

    sz = read(m_epollDescriptor.fd, &exp_cnt, sizeof(exp_cnt));
    if (sz == sizeof(exp_cnt)) {
      status = callback();
    }

    if (!status) {
      stop();
    }

    return status;
  }); // This is our event source dispatch function

  setFinalize([this]() {
    if (m_epollDescriptor.fd > 0) {
      close(m_epollDescriptor.fd);
      m_epollDescriptor.fd = -1;
    }
  });

  if ((m_epollDescriptor.fd = timerfd_create(CLOCK_MONOTONIC, 0)) < 0) {
    throw bswi::except::CreateDescriptor();
  }

  m_epollDescriptor.ev.data.fd = m_epollDescriptor.fd;
  m_epollDescriptor.ev.events = EPOLLIN;
  m_descriptor = std::make_any<EPollDescriptor *>(&m_epollDescriptor);
}

EPTimer::~EPTimer()
{
  if (m_epollDescriptor.fd > 0) {
    close(m_epollDescriptor.fd);
  }
}

auto EPTimer::start(uint64_t usec, bool repeat) -> int
{
  struct itimerspec period = {};
  struct timespec now = {};

  if (clock_gettime(CLOCK_MONOTONIC, &now) == -1) {
    return -1;
  }

  period.it_value.tv_sec = now.tv_sec + (static_cast<long>(usec) / 1000000);
  period.it_value.tv_nsec = now.tv_nsec + (static_cast<long>(usec) % 1000000 * 1000);

  if (repeat) {
    period.it_interval.tv_sec = static_cast<long>(usec / 1000000);
    period.it_interval.tv_nsec = static_cast<long>(usec % 1000000 * 1000);
  } else {
    period.it_interval.tv_sec = 0;
    period.it_interval.tv_nsec = 0;
  }

  if (timerfd_settime(m_epollDescriptor.fd, TFD_TIMER_ABSTIME, &period, nullptr) == -1) {
    return -1;
  }

  m_armed = true;

  return 0;
}

auto EPTimer::stop() -> int
{
  struct itimerspec period = {};

  if (!m_armed) {
    return 0;
  }

  period.it_value.tv_sec = 0;
  period.it_value.tv_nsec = 0;
  period.it_interval.tv_sec = 0;
  period.it_interval.tv_nsec = 0;

  if (timerfd_settime(m_epollDescriptor.fd, TFD_TIMER_ABSTIME, &period, nullptr) == -1) {
    return -1;
  }

  m_armed = false;

  return 0;
}

} // namespace bswi::event
