/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra KeyFile Class
 * @details   INI style key/value parser class
 *-
 */

#include "KeyFile.h"

#include <algorithm>
#include <iostream>
#include <sstream>

using std::cout;
using std::nullopt;
using std::optional;
using std::reference_wrapper;
using std::string;
using std::stringstream;
using std::vector;

namespace bswi::kf
{

static int unique_id = 0;

auto Section::getProperty(const string &key) -> optional<Property>
{
  auto has_property = [&key](const Property &prop) { return prop.key == key; };
  auto result = std::find_if(std::begin(properties), std::end(properties), has_property);

  if (result != std::end(properties)) {
    return optional<reference_wrapper<Property>>{*result};
  }

  return nullopt;
}

auto Section::getValue(const string &key) -> optional<string>
{
  auto has_property = [&key](const Property &prop) { return prop.key == key; };
  auto result = std::find_if(std::begin(properties), std::end(properties), has_property);

  if (result != std::end(properties)) {
    return optional<reference_wrapper<string>>{(*result).value};
  }

  return nullopt;
}

auto KeyFile::parseLine(const string &line) -> int
{
  size_t lineLenght = line.length();
  size_t lpos = 0;
  int status = 0;

  enum class LineType { Blank, Comment, Section, Property } type = LineType::Blank;

  for (const char &c : line) {
    if (c == ' ' || c == '\t' || c == '\n') {
      lpos++;
    } else if (c == ';') {
      type = LineType::Comment;
      break;
    } else if (c == '[') {
      type = LineType::Section;
      break;
    } else {
      type = LineType::Property;
      break;
    }
  }

  switch (type) {
  case LineType::Blank:
  case LineType::Comment:
    break;
  case LineType::Section: {
    bool end = false;
    stringstream name;

    while (lpos < lineLenght && !end) {
      auto c = line.at(lpos);

      if (c == ']') {
        end = true;
      } else if ((c != ' ') && (c != '\t') && (c != '[')) {
        name << c;
      }

      lpos++;
    }

    if (end) {
      auto section = Section(name.str(), unique_id++);
      m_sections.push_back(section);
    } else {
      status = -1;
    }

    break;
  }
  case LineType::Property: {
    bool end = false;
    stringstream key;
    stringstream value;

    while (lpos < lineLenght && !end) {
      auto c = line.at(lpos);

      if (c == '=') {
        end = true;
      } else if ((c != ' ') && (c != '\t')) {
        key << c;
      }

      lpos++;
    }

    if (end) {
      bool firstnonws = false;
      bool hasquotes = false;

      end = false;

      while (lpos < lineLenght && !end) {
        auto c = line.at(lpos);

        if (!firstnonws) {
          if (c != '\n') {
            if ((c != '\"') && (c != '\'')) {
              value << c;
            } else {
              hasquotes = true;
            }
            firstnonws = true;
          }
        } else {
          if (hasquotes && ((c == '\"') || (c == '\''))) {
            end = true;
          } else {
            if (c != '\n') {
              value << c;
            }
          }
        }

        lpos++;
      }

      auto property = Property(key.str(), value.str());

      // If there is no section we add a default
      if (m_sections.empty()) {
        auto section = Section("default", unique_id++);
        m_sections.push_back(section);
      }

      Section &currentSection = m_sections.back();
      currentSection.properties.push_back(property);
    } else {
      status = -1;
    }

    break;
  }
  default:
    status = -1;
    break;
  }

  return status;
}

auto KeyFile::parseFile() -> int
{
  int lineStatus = 0;

  m_inputStream.open(m_inputFile);

  if (!m_inputStream.is_open()) {
    return -1;
  }

  m_sections.clear();

  string line;

  while (getline(m_inputStream, line)) {
    lineStatus += parseLine(line);
  }

  return lineStatus;
}

void KeyFile::printStdOutput()
{
  for (auto &section : getSections()) {
    for (auto &property : section.properties) {
      std::cout << section.name << "[" << section.id << "]"
                << "." << property.key << " = " << property.value << std::endl;
    }
  }
}

auto KeyFile::getSections() -> const vector<Section> &
{
  return m_sections;
}

auto KeyFile::getSections(const string &sectionName) -> vector<Section>
{
  auto has_section = [&sectionName](const Section &section) { return section.name == sectionName; };
  vector<Section> sections;

  std::copy_if(m_sections.begin(), m_sections.end(), std::back_inserter(sections), has_section);

  return sections;
}

auto KeyFile::getSection(const string &sectionName, int id) -> optional<Section>
{
  auto has_section = [&sectionName, &id](const Section &section) {
    return ((section.name == sectionName) && (id < 0 ? true : (section.id == id)));
  };
  auto result = std::find_if(std::begin(m_sections), std::end(m_sections), has_section);

  if (result != std::end(m_sections)) {
    return optional<reference_wrapper<Section>>{*result};
  }

  return nullopt;
}

auto KeyFile::getProperties(const string &sectionName, int id) -> vector<Property>
{
  vector<Property> properties;
  for (auto section : m_sections) {
    if ((section.name == sectionName) && (id < 0 ? true : (section.id == id))) {
      properties.insert(end(properties), begin(section.properties), end(section.properties));
    }
  }

  return properties;
}

auto KeyFile::getProperty(const string &sectionName, int id, const string &key)
    -> optional<Property>
{
  auto has_section = [&sectionName, &id](const Section &section) {
    return ((section.name == sectionName) && (id < 0 ? true : (section.id == id)));
  };
  auto result = std::find_if(std::begin(m_sections), std::end(m_sections), has_section);

  if (result != std::end(m_sections)) {
    return (*result).getProperty(key);
  }

  return nullopt;
}

auto KeyFile::getPropertyValue(const string &sectionName, int id, const string &key)
    -> optional<string>
{
  auto has_section = [&sectionName, &id](const Section &section) {
    return ((section.name == sectionName) && (id < 0 ? true : (section.id == id)));
  };
  auto result = std::find_if(std::begin(m_sections), std::end(m_sections), has_section);

  if (result != std::end(m_sections)) {
    return (*result).getValue(key);
  }

  return nullopt;
}

} // namespace bswi::kf
