/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra KQPollable Class
 * @details   KQueue based Pollable implementation
 *-
 */

#include "KQPollable.h"
#include <any>

using std::function;
using std::string;

namespace bswi::event
{

auto pollableEventsToKqEvents(unsigned events) -> int
{
  int keventEvents = 0;

  if ((events & IPollable::Events::Level) == IPollable::Events::Level) {
    keventEvents |= EVFILT_READ;
  }

  return keventEvents;
}

KQPollable::KQPollable(const string &name,
                       const function<bool()> &callback,
                       int fd,
                       unsigned events,
                       Priority priority)
: IPollable(name, callback, fd, events, priority)
{
  m_keventDescriptor.fd = m_fd;
  m_keventDescriptor.kq = -1;
  m_keventDescriptor.enable = true;

  EV_SET(&m_keventDescriptor.ev,
         m_keventDescriptor.fd,
         pollableEventsToKqEvents(m_events),
         0,
         0,
         0,
         nullptr);

  m_descriptor = std::make_any<KEventDescriptor *>(&m_keventDescriptor);
}

void KQPollable::lateSetup(const std::function<bool()> &callback,
                           int fd,
                           unsigned events,
                           Priority priority)
{
  IPollable::lateSetup(callback, fd, events, priority);

  m_keventDescriptor.fd = m_fd;
  m_keventDescriptor.kq = -1;
  m_keventDescriptor.enable = true;

  EV_SET(&m_keventDescriptor.ev,
         m_keventDescriptor.fd,
         pollableEventsToKqEvents(m_events),
         0,
         0,
         0,
         nullptr);

  m_descriptor = &m_keventDescriptor;
}

} // namespace bswi::event
