/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra EPTimer Class
 * @details   EPoll based Timer implementation
 *-
 */

#pragma once

#include "EPTypes.h"
#include "ITimer.h"

namespace bswi::event
{

class EPTimer : public ITimer
{
public:
  explicit EPTimer(const std::string &name,
                   const std::function<bool()> &callback,
                   Priority priority = Priority::Normal);
  virtual ~EPTimer();

  auto start(uint64_t usec, bool repeat) -> int final;
  auto stop() -> int final;

private:
  EPollDescriptor m_epollDescriptor = {};
};

} // namespace bswi::event
