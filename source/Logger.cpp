/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra Logger Class
 * @details   Logger class with build time backend selection
 *-
 */

#include "Logger.h"
#ifdef WITH_SYSLOG
#include <syslog.h>
#endif
#ifdef WITH_JOURNALD
#include <systemd/sd-journal.h>
#endif

#include <chrono>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <sstream>

using std::string;

namespace bswi::log
{

Logger *Logger::instance = nullptr;
string Logger::m_appId;
Logger::Message::Type Logger::m_level = Logger::Message::Type::Verbose;

static auto getDate() -> std::string
{
  const auto now = std::chrono::system_clock::now();
  const auto ms =
      std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) % 1000;
  const auto timer = std::chrono::system_clock::to_time_t(now);

  const std::tm bt = *std::localtime(&timer);
  std::ostringstream oss;

  oss << std::put_time(&bt, "%F %T"); // %Y-%m-%d HH:MM:SS
  oss << '.' << std::setfill('0') << std::setw(3) << ms.count();

  return oss.str();
}

static void printConsole(Logger::Message::Type type, const string &appId, const string &pld)
{
  string level;

  switch (type) {
  case Logger::Message::Type::Verbose:
    level = "Verbose";
    break;
  case Logger::Message::Type::Debug:
    level = "Debug  ";
    break;
  case Logger::Message::Type::Info:
    level = "Info   ";
    break;
  case Logger::Message::Type::Notice:
    level = "Notice ";
    break;
  case Logger::Message::Type::Warn:
    level = "Warning";
    break;
  case Logger::Message::Type::Error:
    level = "Error  ";
    break;
  case Logger::Message::Type::Fatal:
    level = "Fatal  ";
    break;
  }

  std::cout << "|" << getDate() << "|" << appId << "|" << level << "\t| " << pld << '\n';
}

#ifdef WITH_SYSLOG
static void printSyslog(Logger::Message::Type type, const string &, const string &pld)
{
  int level = LOG_INFO;

  switch (type) {
  case Logger::Message::Type::Verbose:
  case Logger::Message::Type::Debug:
    level = LOG_DEBUG;
    break;
  case Logger::Message::Type::Info:
    level = LOG_INFO;
    break;
  case Logger::Message::Type::Notice:
    level = LOG_NOTICE;
    break;
  case Logger::Message::Type::Warn:
    level = LOG_WARNING;
    break;
  case Logger::Message::Type::Error:
    level = LOG_ERR;
    break;
  case Logger::Message::Type::Fatal:
    level = LOG_ALERT;
    break;
  }

  syslog(level, "%s", pld.c_str());
}
#endif
#ifdef WITH_JOURNALD
static void printJournald(Logger::Message::Type type, const string &, const string &pld)
{
  int level = LOG_INFO;

  switch (type) {
  case Logger::Message::Type::Verbose:
  case Logger::Message::Type::Debug:
    level = LOG_DEBUG;
    break;
  case Logger::Message::Type::Info:
    level = LOG_INFO;
    break;
  case Logger::Message::Type::Notice:
    level = LOG_NOTICE;
    break;
  case Logger::Message::Type::Warn:
    level = LOG_WARNING;
    break;
  case Logger::Message::Type::Error:
    level = LOG_ERR;
    break;
  case Logger::Message::Type::Fatal:
    level = LOG_ALERT;
    break;
  }

  sd_journal_print(level, "%s", pld.c_str());
}
#endif

void Logger::Message::print()
{
  if (m_type >= Logger::getLogLevel()) {
#ifdef WITH_SYSLOG
    printSyslog(m_type, m_appId, m_stream.str());
#elif WITH_JOURNALD
    printJournald(m_type, m_appId, m_stream.str());
#else
    printConsole(m_type, m_appId, m_stream.str());
#endif
  }
}

void Logger::registerApplication(const string &appId, const string & /*description*/)
{
  m_appId = appId;
#ifdef WITH_SYSLOG
  openlog(m_appId.c_str(), LOG_PID, LOG_USER);
#endif
}

} // namespace bswi::log
