/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra ITimer Class
 * @details   Timer interface class
 *-
 */

#pragma once

#include <atomic>

#include "IEventSource.h"

namespace bswi::event
{

class ITimer : public IEventSource
{
public:
  explicit ITimer(const std::string &name,
                  const std::function<bool()> &callback,
                  Priority priority = Priority::Normal)
  {
    m_name = name;
    m_priority = priority;
    setDispatch(callback);
    setPrepare([]() { return true; });
    setCheck([](int) { return true; });
  }

  virtual auto start(uint64_t usec, bool repeat) -> int = 0;
  virtual auto stop() -> int = 0;

  [[maybe_unused]] bool isArmed() { return m_armed; }

protected:
  std::atomic<bool> m_armed = false;
};

} // namespace bswi::event
