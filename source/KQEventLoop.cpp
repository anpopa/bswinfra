/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra KQEventLoop Class
 * @details   KQueue based EventLoop implementation class
 *-
 */

#include "KQEventLoop.h"
#include "Exceptions.h"
#include "KQTypes.h"

#include <thread>
#include <unistd.h>

using std::any_cast;
using std::mutex;
using std::shared_ptr;
using std::string;
using std::thread;
using std::unique_lock;

namespace bswi::event
{

static void event_worker(void *event_loop)
{
  auto *eventLoop = reinterpret_cast<KQEventLoop *>(event_loop);
  struct kevent events[eventLoop->m_max_events];
  struct timespec timeout {
  };

  if (eventLoop->isStarted()) {
    return;
  }

  eventLoop->setStarted(true);
  eventLoop->setRunning(true);

  timeout.tv_sec = eventLoop->m_pollInterval / 1000;
  timeout.tv_nsec = static_cast<long>((eventLoop->m_pollInterval % 1000) * 1000000);

  do {
    int eventCount =
        kevent(eventLoop->getKqueue(), nullptr, 0, events, eventLoop->m_max_events, &timeout);

    if (eventCount == -1) {
      break;
    }

    if (eventCount == 0) {
      continue;
    }

    // Commit any changes to our lists during last loop
    eventLoop->getHighPriorityQueue().commit();
    eventLoop->getNormalPriorityQueue().commit();

    eventLoop->getHighPriorityQueue().foreach ([](const std::shared_ptr<IEventSource> source) {
      source->setReady(source->prepare());
      source->setPolled(false);
    });

    eventLoop->getNormalPriorityQueue().foreach ([](const std::shared_ptr<IEventSource> source) {
      source->setReady(source->prepare());
      source->setPolled(false);
    });

    for (int i = 0; i < eventCount; i++) {
      bool foundValid = false;

      eventLoop->getHighPriorityQueue().foreach ([i, &events, &foundValid](
                                                     const std::shared_ptr<IEventSource> source) {
        if (any_cast<KEventDescriptor *>(source->getDescriptor())->ev.ident == events[i].ident) {
          if (source->check(events[i].flags)) {
            source->setPolled(true);
            foundValid = true;
          }
        }
      });

      if (foundValid) {
        continue;
      }

      eventLoop->getNormalPriorityQueue().foreach ([i, &events, &foundValid](
                                                       const std::shared_ptr<IEventSource> source) {
        if (any_cast<KEventDescriptor *>(source->getDescriptor())->ev.ident == events[i].ident) {
          if (source->check(events[i].flags)) {
            source->setPolled(true);
            foundValid = true;
          }
        }
      });

      if (foundValid) {
        continue;
      }

      // Remove sporious event
      events[i].flags = EV_DELETE;
      kevent(eventLoop->getKqueue(), &events[i], 1, nullptr, 0, nullptr);
    }

    eventLoop->getHighPriorityQueue().foreach (
        [eventLoop](const std::shared_ptr<IEventSource> source) {
          if (source->isPolled() && source->isReady()) {
            if (!source->dispatch()) {
              eventLoop->removeSource(source);
            }
          }
        });

    eventLoop->getNormalPriorityQueue().foreach (
        [eventLoop](const std::shared_ptr<IEventSource> source) {
          if (source->isPolled() && source->isReady()) {
            if (!source->dispatch()) {
              eventLoop->removeSource(source);
            }
          }
        });
  } while (true);

  eventLoop->setRunning(false);

  if (eventLoop->ownThread()) {
    unique_lock<mutex> lk(eventLoop->runThreadMutex());
    eventLoop->runThreadCond().notify_one();
  }
}

KQEventLoop::KQEventLoop(const string &name, size_t poll_usec, bool own_thread)
: IEventLoop(name, poll_usec, own_thread)
{
  if ((m_kqFd = kqueue()) < 0) {
    throw bswi::except::CreateDescriptor();
  }
}

KQEventLoop::~KQEventLoop()
{
  stop();
}

auto KQEventLoop::addSource(const shared_ptr<IEventSource> source, IEventSource::Priority priority)
    -> int
{
  auto *d = any_cast<KEventDescriptor *>(source->getDescriptor());

  d->kq = m_kqFd;
  if (d->enable) {
    d->ev.flags = EV_ADD | EV_CLEAR | EV_ENABLE | EV_EOF;
  }

  if (priority != IEventSource::Priority::Normal) {
    source->setPriority(priority);
  }
  if (source->getPriority() == IEventSource::Priority::High) {
    getHighPriorityQueue().append(source);
    if (d->enable) {
      if (kevent(m_kqFd, &d->ev, 1, nullptr, 0, nullptr) == -1) {
        getHighPriorityQueue().remove(source);
        return -1;
      }
    }
  } else {
    getNormalPriorityQueue().append(source);
    if (d->enable) {
      if (kevent(m_kqFd, &d->ev, 1, nullptr, 0, nullptr) == -1) {
        getNormalPriorityQueue().remove(source);
        return -1;
      }
    }
  }

  return 0;
}

auto KQEventLoop::removeSource(const shared_ptr<IEventSource> source) -> int
{
  auto *d = any_cast<KEventDescriptor *>(source->getDescriptor());

  d->ev.flags = EV_DELETE;
  auto status = kevent(m_kqFd, &d->ev, 1, nullptr, 0, nullptr);
  source->finalize(); // let source finalize before we remove reference

  if (source->getPriority() == IEventSource::Priority::High) {
    getHighPriorityQueue().remove(source);
  } else {
    getNormalPriorityQueue().remove(source);
  }

  return status;
}

auto KQEventLoop::removeSource(const string &sourceName) -> int
{
  bool found = false;
  int status = -1;

  getHighPriorityQueue().foreach (
      [this, &status, &found, &sourceName](const std::shared_ptr<IEventSource> source) {
        if (source->getName() == sourceName) {
          status = removeSource(source);
          found = true;
        }
      });

  if (found) {
    return status;
  }

  getNormalPriorityQueue().foreach (
      [this, &status, &found, &sourceName](const std::shared_ptr<IEventSource> source) {
        if (source->getName() == sourceName) {
          status = removeSource(source);
          found = true;
        }
      });

  return status;
}

void KQEventLoop::triggerSource(const string &sourceName)
{
  getHighPriorityQueue().foreach ([&sourceName](const std::shared_ptr<IEventSource> source) {
    if (source->getName() == sourceName) {
      source->trigger();
    }
  });

  getNormalPriorityQueue().foreach ([&sourceName](const std::shared_ptr<IEventSource> source) {
    if (source->getName() == sourceName) {
      source->trigger();
    }
  });
}

void KQEventLoop::start()
{
  if (isStarted()) {
    return;
  }

  if (m_ownThread) {
    thread worker(event_worker, this);
    worker.join();
  } else {
    event_worker(this);
  }
}

void KQEventLoop::stop()
{
  if (isStarted()) {
    if (m_kqFd > 0) {
      close(m_kqFd);
      m_kqFd = -1;
    }

    if (ownThread()) {
      unique_lock<mutex> lk(runThreadMutex());
      runThreadCond().wait(lk);
    }

    setStarted(false);
  }
}

} // namespace bswi::event
