/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra IApplication Class
 * @details   Application interface class
 *-
 */

#pragma once

#include <any>
#include <functional>
#include <string>

#include "EventLoop.h"
#include "Logger.h"

using namespace bswi::event;
using namespace bswi::log;

namespace bswi::app
{

class IApplication
{
public:
  explicit IApplication(const std::string &name, const std::string &description)
  {
    Logger::getInstance()->registerApplication(name, description);
    m_mainEventLoop = std::make_unique<EventLoop>(name, 1000, false);
  }
  virtual ~IApplication() { m_mainEventLoop.reset(); }

  void addEventSource(const std::shared_ptr<IEventSource> source,
                      IEventSource::Priority priority = IEventSource::Priority::Normal)
  {
    m_mainEventLoop->addSource(source, priority);
  }

  auto remEventSource(const std::string &sourceName) -> int
  {
    return m_mainEventLoop->removeSource(sourceName);
  }

  auto remEventSource(const std::shared_ptr<IEventSource> source) -> int
  {
    return m_mainEventLoop->removeSource(source);
  }

  virtual void run()
  {
    if (m_running) {
      return;
    }

    m_running = true;
    m_mainEventLoop->start();
    m_running = false;
  }

  virtual void stop() = 0;

public:
  IApplication(IApplication const &) = delete;
  void operator=(IApplication const &) = delete;

protected:
  std::unique_ptr<EventLoop> m_mainEventLoop = nullptr;
  std::atomic<bool> m_running = false;
};

} // namespace bswi::app
