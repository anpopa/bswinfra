/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra IPollable Class
 * @details   Pollable interface class
 *-
 */

#pragma once

#include <atomic>

#include "IEventSource.h"

namespace bswi::event
{

class IPollable : public IEventSource
{
public:
  typedef enum Events {
    Level = 0x00000001,
    Edge = 0x00000002,
    OneShot = 0x00000004,
    Wakeup = 0x00000008
  } Events;

  explicit IPollable(const std::string &name) { m_name = name; }
  explicit IPollable(const std::string &name, int fd)
  {
    m_name = name;
    m_fd = fd;
  }
  explicit IPollable(const std::string &name,
                     const std::function<bool()> &callback,
                     int fd,
                     unsigned events,
                     Priority priority = Priority::Normal)
  {
    m_name = name;
    m_fd = fd;
    m_events = events;
    m_priority = priority;
    setDispatch(callback);
    setPrepare([]() { return true; });
    setCheck([](int) { return true; });
  }

  virtual void
  lateSetup(const std::function<bool()> &callback, int fd, unsigned events, Priority priority)
  {
    m_fd = fd;
    m_events = events;
    m_priority = priority;
    setDispatch(callback);
    setPrepare([]() { return true; });
    setCheck([](int) { return true; });
  }

public:
  IPollable() = delete;

protected:
  int m_fd{};
  unsigned m_events{};
};

} // namespace bswi::event
