/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra EPEventLoop Class
 * @details   EPoll based EventLoop implementation class
 *-
 */

#include "EPEventLoop.h"
#include "EPTypes.h"
#include "Exceptions.h"
#include "Logger.h"

#include <errno.h>
#include <iostream>
#include <thread>
#include <unistd.h>

using std::any_cast;
using std::mutex;
using std::shared_ptr;
using std::string;
using std::thread;
using std::unique_lock;

namespace bswi::event
{

static void event_worker(void *event_loop)
{
  auto *eventLoop = reinterpret_cast<EPEventLoop *>(event_loop);
  struct epoll_event events[eventLoop->m_max_events];

  if (eventLoop->isStarted()) {
    return;
  }

  eventLoop->setStarted(true);
  eventLoop->setRunning(true);

  do {
    int eventCount = epoll_wait(eventLoop->getEpollFD(),
                                events,
                                static_cast<int>(eventLoop->m_max_events),
                                static_cast<int>(eventLoop->m_pollInterval));

    if ((eventCount == -1) && (errno != EINTR)) {
      break;
    }

    if (eventCount == 0) {
      continue;
    }

    // Commit any changes to our lists during last loop
    eventLoop->getHighPriorityQueue().commit();
    eventLoop->getNormalPriorityQueue().commit();

    eventLoop->getHighPriorityQueue().foreach ([](const std::shared_ptr<IEventSource> source) {
      source->setReady(source->prepare());
      source->setPolled(false);
    });

    eventLoop->getNormalPriorityQueue().foreach ([](const std::shared_ptr<IEventSource> source) {
      source->setReady(source->prepare());
      source->setPolled(false);
    });

    for (int i = 0; i < eventCount; i++) {
      bool foundValid = false;

      eventLoop->getHighPriorityQueue().foreach (
          [i, &events, &foundValid](const std::shared_ptr<IEventSource> source) {
            if (any_cast<EPollDescriptor *>(source->getDescriptor())->fd == events[i].data.fd) {
              if (source->check(events[i].events)) {
                source->setPolled(true);
                foundValid = true;
              }
            }
          });

      if (foundValid) {
        continue;
      }

      eventLoop->getNormalPriorityQueue().foreach (
          [i, &events, &foundValid](const std::shared_ptr<IEventSource> source) {
            if (any_cast<EPollDescriptor *>(source->getDescriptor())->fd == events[i].data.fd) {
              if (source->check(events[i].events)) {
                source->setPolled(true);
                foundValid = true;
              }
            }
          });

      if (foundValid) {
        continue;
      }

      // Remove spurious event
      epoll_ctl(eventLoop->getEpollFD(), EPOLL_CTL_DEL, events[i].data.fd, nullptr);
    }

    eventLoop->getHighPriorityQueue().foreach (
        [eventLoop](const std::shared_ptr<IEventSource> source) {
          if (source->isPolled() && source->isReady()) {
            if (!source->dispatch()) {
              eventLoop->removeSource(source);
            }
          }
        });

    eventLoop->getNormalPriorityQueue().foreach (
        [eventLoop](const std::shared_ptr<IEventSource> source) {
          if (source->isPolled() && source->isReady()) {
            if (!source->dispatch()) {
              eventLoop->removeSource(source);
            }
          }
        });
  } while (true);

  eventLoop->setRunning(false);

  if (eventLoop->ownThread()) {
    unique_lock<mutex> lk(eventLoop->runThreadMutex());
    eventLoop->runThreadCond().notify_one();
  }
}

EPEventLoop::EPEventLoop(const string &name, size_t poll_usec, bool own_thread)
: IEventLoop(name, poll_usec, own_thread)
{
  if ((m_epollFd = epoll_create1(0 | EPOLL_CLOEXEC)) < 0) {
    throw bswi::except::CreateDescriptor();
  }
}

EPEventLoop::~EPEventLoop()
{
  stop();
}

auto EPEventLoop::addSource(const shared_ptr<IEventSource> source, IEventSource::Priority priority)
    -> int
{
  int fd = any_cast<EPollDescriptor *>(source->getDescriptor())->fd;
  struct epoll_event *ev = &any_cast<EPollDescriptor *>(source->getDescriptor())->ev;

  if (priority != IEventSource::Priority::Normal) {
    source->setPriority(priority);
  }
  if (source->getPriority() == IEventSource::Priority::High) {
    getHighPriorityQueue().append(source);
    if (epoll_ctl(m_epollFd, EPOLL_CTL_ADD, fd, ev) == -1) {
      getHighPriorityQueue().remove(source);
      return -1;
    }
  } else {
    getNormalPriorityQueue().append(source);
    if (epoll_ctl(m_epollFd, EPOLL_CTL_ADD, fd, ev) == -1) {
      getNormalPriorityQueue().remove(source);
      return -1;
    }
  }

  return 0;
}

auto EPEventLoop::removeSource(const shared_ptr<IEventSource> source) -> int
{
  int fd = any_cast<EPollDescriptor *>(source->getDescriptor())->fd;
  struct epoll_event *ev = &any_cast<EPollDescriptor *>(source->getDescriptor())->ev;

  auto status = epoll_ctl(m_epollFd, EPOLL_CTL_DEL, fd, ev);
  source->finalize(); // let source finalize before we remove reference

  if (source->getPriority() == IEventSource::Priority::High) {
    getHighPriorityQueue().remove(source);
  } else {
    getNormalPriorityQueue().remove(source);
  }

  return status;
}

auto EPEventLoop::removeSource(const string &sourceName) -> int
{
  bool found = false;
  int status = -1;

  getHighPriorityQueue().foreach (
      [this, &status, &found, &sourceName](const std::shared_ptr<IEventSource> source) {
        if (source->getName() == sourceName) {
          status = removeSource(source);
          found = true;
        }
      });

  if (found) {
    return status;
  }

  getNormalPriorityQueue().foreach (
      [this, &status, &found, &sourceName](const std::shared_ptr<IEventSource> source) {
        if (source->getName() == sourceName) {
          status = removeSource(source);
          found = true;
        }
      });

  return status;
}

void EPEventLoop::triggerSource(const string &sourceName)
{
  getHighPriorityQueue().foreach ([&sourceName](const std::shared_ptr<IEventSource> source) {
    if (source->getName() == sourceName) {
      source->trigger();
    }
  });

  getNormalPriorityQueue().foreach ([&sourceName](const std::shared_ptr<IEventSource> source) {
    if (source->getName() == sourceName) {
      source->trigger();
    }
  });
}

void EPEventLoop::start()
{
  if (isStarted()) {
    return;
  }

  if (m_ownThread) {
    thread worker(event_worker, this);
    worker.join();
  } else {
    event_worker(this);
  }
}

void EPEventLoop::stop()
{
  if (isStarted()) {
    if (m_epollFd > 0) {
      close(m_epollFd);
      m_epollFd = -1;
    }

    if (ownThread()) {
      unique_lock<mutex> lk(runThreadMutex());
      runThreadCond().wait(lk);
    }

    setStarted(false);
  }
}

} // namespace bswi::event
