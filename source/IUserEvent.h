/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra IUserEvent Class
 * @details   UserEvent interface class
 *-
 */

#pragma once

#include <atomic>

#include "IEventSource.h"

namespace bswi::event
{

class IUserEvent : public IEventSource
{
public:
  explicit IUserEvent(const std::string &name,
                      const std::function<bool()> &callback,
                      Priority priority = Priority::Normal)
  : m_callback(callback)
  {
    m_name = name;
    m_priority = priority;
    setPrepare([]() { return true; });
    setCheck([](int) { return true; });
  }

  explicit IUserEvent(const std::string &name, Priority priority = Priority::Normal)
  {
    m_name = name;
    m_priority = priority;
    setPrepare([]() { return true; });
    setCheck([](int) { return true; });
  }

  void setCallback(const std::function<bool()> &callback) { m_callback = callback; }

protected:
  std::function<bool()> m_callback;
};

} // namespace bswi::event
