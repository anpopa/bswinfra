/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra KQEventLoop Class
 * @details   KQueue based EventLoop implementation class
 *-
 */

#pragma once

#include <condition_variable>
#include <mutex>

#include "IEventLoop.h"

namespace bswi::event
{

class KQEventLoop : public IEventLoop
{
public:
  explicit KQEventLoop(const std::string &name, std::size_t poll_usec, bool own_thread = true);
  virtual ~KQEventLoop();

  auto addSource(const std::shared_ptr<IEventSource> source, IEventSource::Priority priority)
      -> int final;
  auto removeSource(const std::shared_ptr<IEventSource> source) -> int final;
  auto removeSource(const std::string &sourceName) -> int final;
  void triggerSource(const std::string &sourceName) final;
  void start() final;
  void stop() final;

public:
  void setStarted(bool state) { m_started = state; }
  bool isStarted() { return m_started; }
  auto getKqueue() -> int const { return m_kqFd; }
  std::condition_variable &runThreadCond() { return m_runThreadCond; }
  std::mutex &runThreadMutex() { return m_runThreadMutex; }

private:
  std::condition_variable m_runThreadCond;
  std::mutex m_runThreadMutex;
  std::atomic<bool> m_started = false;
  int m_kqFd = -1;
};

} // namespace bswi::event
