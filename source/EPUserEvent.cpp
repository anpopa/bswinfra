/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra EPUserEvent Class
 * @details   EPoll based UserEvent implementation
 *-
 */

#include "EPUserEvent.h"
#include "Exceptions.h"

#include <cstdint>
#include <sys/eventfd.h>
#include <unistd.h>

#include <cstdlib>

using std::function;
using std::string;

namespace bswi::event
{

EPUserEvent::EPUserEvent(const string &name, const function<bool()> &callback, Priority priority)
: IUserEvent(name, callback, priority)
{
  lateSetup();
}

EPUserEvent::EPUserEvent(const string &name, Priority priority)
: IUserEvent(name, priority)
{
  lateSetup();
}

void EPUserEvent::lateSetup()
{
  setDispatch([this]() {
    uint64_t exp_cnt = 0;
    bool status = false;
    ssize_t sz;

    sz = read(m_epollDescriptor.fd, &exp_cnt, sizeof(exp_cnt));
    if (sz == sizeof(exp_cnt)) {
      uint64_t i = 1;

      do {
        if (m_callback != nullptr) {
          status = m_callback();
        } else {
          status = true;
        }
      } while ((i++ < exp_cnt) && status);
    }

    return status;
  }); // This is our event source dispatch function

  setFinalize([this]() {
    if (m_epollDescriptor.fd > 0) {
      close(m_epollDescriptor.fd);
      m_epollDescriptor.fd = -1;
    }
  });

  setTrigger([this]() {
    const uint64_t cnt = 1;
    ssize_t len;

    len = write(m_epollDescriptor.fd, &cnt, sizeof(cnt));
    if (len != static_cast<ssize_t>(sizeof(cnt))) {
      return false;
    }

    return true;
  });

  if ((m_epollDescriptor.fd = eventfd(0, 0)) < 0) {
    throw bswi::except::CreateDescriptor();
  }

  m_epollDescriptor.ev.data.fd = m_epollDescriptor.fd;
  m_epollDescriptor.ev.events = EPOLLIN;
  m_descriptor = std::make_any<EPollDescriptor *>(&m_epollDescriptor);
}

EPUserEvent::~EPUserEvent()
{
  if (m_epollDescriptor.fd > 0) {
    close(m_epollDescriptor.fd);
  }
}

} // namespace bswi::event
