/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra EPUserEvent Class
 * @details   EPoll based UserEvent implementation
 *-
 */

#pragma once

#include "EPTypes.h"
#include "IUserEvent.h"

namespace bswi::event
{

class EPUserEvent : public IUserEvent
{
public:
  explicit EPUserEvent(const std::string &name,
                       const std::function<bool()> &callback,
                       Priority priority = Priority::Normal);
  explicit EPUserEvent(const std::string &name, Priority priority = Priority::Normal);
  virtual ~EPUserEvent();

private:
  void lateSetup();

private:
  EPollDescriptor m_epollDescriptor = {};
};

} // namespace bswi::event
