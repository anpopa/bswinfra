/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra EPAsyncQueue Class
 * @details   EPoll based AsyncQueue implementation class
 *-
 */

#pragma once

#include "EPTypes.h"
#include "Exceptions.h"
#include "IAsyncQueue.h"

#include <cstdint>
#include <cstdlib>
#include <mutex>
#include <queue>
#include <sys/eventfd.h>
#include <unistd.h>

namespace bswi::event
{

template <class T>
class EPAsyncQueue : public IAsyncQueue<T>
{
public:
  explicit EPAsyncQueue(const std::string &name,
                        const std::function<bool(const T &data)> &callback,
                        IEventSource::Priority priority = IEventSource::Priority::Normal)
  : IAsyncQueue<T>(name, callback, priority)
  {
    IEventSource::setDispatch([this, callback]() {
      uint64_t exp_cnt = 0;
      bool status = false;
      ssize_t sz;

      sz = read(m_epollDescriptor.fd, &exp_cnt, sizeof(exp_cnt));
      if (sz == sizeof(exp_cnt)) {
        uint64_t i = 1;

        do {
          status = callback(m_queue.front());
          std::scoped_lock lk(m_mutex);
          m_queue.pop();
        } while ((i++ < exp_cnt) && status);
      }

      return status;
    }); // This is our event source dispatch function

    IEventSource::setFinalize([this]() {
      if (m_epollDescriptor.fd > 0) {
        close(m_epollDescriptor.fd);
        m_epollDescriptor.fd = -1;
      }
    });

    IEventSource::setTrigger([this]() {
      const uint64_t cnt = 1;
      ssize_t len;

      len = write(m_epollDescriptor.fd, &cnt, sizeof(cnt));
      if (len != static_cast<ssize_t>(sizeof(cnt))) {
        return false;
      }

      return true;
    });

    if ((m_epollDescriptor.fd = eventfd(0, 0)) < 0) {
      throw bswi::except::CreateDescriptor();
    }

    m_epollDescriptor.ev.data.fd = m_epollDescriptor.fd;
    m_epollDescriptor.ev.events = EPOLLIN;
    IEventSource::m_descriptor = std::make_any<EPollDescriptor *>(&m_epollDescriptor);
  }

  virtual ~EPAsyncQueue()
  {
    if (m_epollDescriptor.fd > 0) {
      close(m_epollDescriptor.fd);
    }
  }

  bool push(const T &val) final
  {
    std::scoped_lock lk(m_mutex);
    m_queue.push(val);
    return IEventSource::trigger();
  }

private:
  std::queue<T> m_queue;
  std::mutex m_mutex;
  EPollDescriptor m_epollDescriptor = {};
};

} // namespace bswi::event
