/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra KQPathEvent Class
 * @details   KQueue based PathEvent implementation
 *-
 */

#include "KQPathEvent.h"
#include "Exceptions.h"

#include <fcntl.h>
#include <unistd.h>

using std::function;
using std::string;

namespace bswi::event
{

using Event = IPathEvent::Events;
using Flag = IPathEvent::Flags;

auto PE2KE(unsigned events) -> unsigned
{
  unsigned kqEvents = 0;

  if ((events & Event::Attrib) == Event::Attrib) {
    kqEvents |= NOTE_ATTRIB;
  }
#ifndef __APPLE__
  if ((events & Event::Close) == Event::Close) {
    kqEvents |= NOTE_CLOSE;
    kqEvents |= NOTE_CLOSE_WRITE;
  }
  if ((events & Event::CloseWrite) == Event::CloseWrite) {
    kqEvents |= NOTE_CLOSE_WRITE;
  }
  if ((events & Event::CloseNoWrite) == Event::CloseNoWrite) {
    kqEvents |= NOTE_CLOSE;
  }
#endif
  if ((events & Event::Create) == Event::Create) {
    kqEvents |= NOTE_LINK;
    kqEvents |= NOTE_WRITE;
  }
  if ((events & Event::Delete) == Event::Delete) {
    kqEvents |= NOTE_EXTEND;
  }
  if ((events & Event::DeleteSelf) == Event::DeleteSelf) {
    kqEvents |= NOTE_DELETE;
  }
  if ((events & Event::Modify) == Event::Modify) {
    kqEvents |= NOTE_WRITE;
  }
  if ((events & Event::Move) == Event::Move) {
    kqEvents |= NOTE_EXTEND;
  }
  if ((events & Event::MoveSelf) == Event::MoveSelf) {
    kqEvents |= NOTE_RENAME;
  }

  return kqEvents;
}

auto KE2PE(unsigned events) -> unsigned
{
  unsigned pathEvents = 0;

  if ((events & NOTE_ATTRIB) == NOTE_ATTRIB) {
    pathEvents |= Event::Attrib;
  }
  if (((events & NOTE_LINK) == NOTE_LINK) || ((events & NOTE_WRITE) == NOTE_WRITE)) {
    pathEvents |= Event::Create;
  }
  if ((events & NOTE_EXTEND) == NOTE_EXTEND) {
    pathEvents |= Event::Delete;
    pathEvents |= Event::Move;
  }
  if ((events & NOTE_DELETE) == NOTE_DELETE) {
    pathEvents |= Event::DeleteSelf;
  }
  if ((events & NOTE_WRITE) == NOTE_WRITE) {
    pathEvents |= Event::Modify;
  }
  if ((events & NOTE_RENAME) == NOTE_RENAME) {
    pathEvents |= Event::MoveSelf;
  }
#ifndef __APPLE__
  if ((events & NOTE_CLOSE) == NOTE_CLOSE) {
    pathEvents |= Event::CloseNoWrite;
  }
  if ((events & NOTE_CLOSE_WRITE) == NOTE_CLOSE_WRITE) {
    pathEvents |= Event::CloseWrite;
  }
#endif

  return pathEvents;
}

auto PF2KF(unsigned flags) -> unsigned
{
  unsigned kqFlags = 0;

  if ((flags & Flag::NonBlock) == Flag::NonBlock) {
    kqFlags |= O_NONBLOCK;
  }
  if ((flags & Flag::CloseExec) == Flag::CloseExec) {
    kqFlags |= 0;
  }

  return kqFlags;
}

[[maybe_unused]] auto KF2PF(unsigned flags) -> unsigned
{
  unsigned pathFlags = O_RDONLY;

  if ((flags & O_NONBLOCK) == O_NONBLOCK) {
    pathFlags |= Flag::NonBlock;
  }

  return pathFlags;
}

KQPathEvent::KQPathEvent(const string &name,
                         const function<bool(const string &path, unsigned /*eventmask*/)> &callback,
                         unsigned flags,
                         Priority priority)
: IPathEvent(name, callback, flags, priority)
{
  setDispatch([this, callback]() {
    bool status = true;
    int eventCount;

    eventCount = kevent(m_keventDescriptor.fd, nullptr, 0, m_events, m_max_events, nullptr);

    for (int i = 0; i < eventCount && status; i++) {
      for (auto const &watch : m_watches) {
        if (watch.second == m_events[i].ident) {
          status = callback(watch.first, KE2PE(m_events[i].fflags));
        }
      }
    }

    return status;
  }); // This is our event source dispatch function

  setFinalize([this]() {
    if (m_keventDescriptor.kq != -1) {
      m_keventDescriptor.ev.flags = EV_DELETE;
      kevent(m_keventDescriptor.kq, &m_keventDescriptor.ev, 1, nullptr, 0, nullptr);
    }
  });

  m_flags = flags;
  m_keventDescriptor.kq = -1;
  m_keventDescriptor.enable = true;

  m_keventDescriptor.fd = kqueue();
  if (m_keventDescriptor.fd < 0) {
    throw bswi::except::CreateDescriptor();
  }

  EV_SET(&m_keventDescriptor.ev, m_keventDescriptor.fd, EVFILT_READ, 0, 0, 0, nullptr);
  m_descriptor = std::make_any<KEventDescriptor *>(&m_keventDescriptor);
}

KQPathEvent::~KQPathEvent()
{
  for (auto &watch : m_watches) {
    if (watch.second > 0) {
      close(watch.second);
      watch.second = -1;
    }
  }

  if (m_keventDescriptor.fd > 0) {
    close(m_keventDescriptor.fd);
  }
}

auto KQPathEvent::addWatch(const string &path, unsigned eventMask) -> int
{
  struct kevent event = {0};

  std::scoped_lock lk(m_watchesMutex);

  auto search = m_watches.find(path);
  int fd;

  if (search != m_watches.end()) {
    fd = search->second;
  } else {
    fd = open(path.c_str(), static_cast<int>(PF2KF(m_flags)));
  }

  if (fd < 0) {
    return -1;
  }

  EV_SET(&event,
         fd,
         EVFILT_VNODE,
         EV_ADD | EV_CLEAR | EV_ENABLE | EV_EOF,
         PE2KE(eventMask),
         0,
         nullptr);

  if (kevent(m_keventDescriptor.fd, &event, 1, nullptr, 0, nullptr) < 0) {
    close(fd);
    return -1;
  }

  m_watches.insert_or_assign(path, fd);

  return fd;
}

auto KQPathEvent::remWatch(const string &path) -> int
{
  struct kevent event = {0};
  int status = -1;

  std::scoped_lock lk(m_watchesMutex);

  auto search = m_watches.find(path);

  if (search != m_watches.end()) {
    EV_SET(&event, search->second, EVFILT_VNODE, EV_DELETE, 0, 0, nullptr);
    if (kevent(m_keventDescriptor.fd, &event, 1, nullptr, 0, nullptr) < 0) {
      status = -1;
    }
    close(search->second);
    m_watches.erase(search->first);
  }

  return status;
}

} // namespace bswi::event
