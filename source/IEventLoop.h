/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra IEventLoop Class
 * @details   EventLoop interface class
 *-
 */

#pragma once

#include <atomic>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "IEventSource.h"
#include "SafeList.h"

namespace bswi::event
{

class IEventLoop
{
public:
  explicit IEventLoop(std::string name, std::size_t poll_usec, bool ownThread = true)
  : m_pollInterval(poll_usec)
  , m_name(std::move(name))
  , m_ownThread(ownThread){};

  bool isRunning() { return m_running; }

  void setRunning(bool state) { m_running = state; }

  [[nodiscard]] bool ownThread() const { return m_ownThread; }

  virtual auto addSource(const std::shared_ptr<IEventSource> source,
                         IEventSource::Priority priority) -> int = 0;

  virtual auto removeSource(const std::shared_ptr<IEventSource> source) -> int = 0;
  virtual auto removeSource(const std::string &sourceName) -> int = 0;
  virtual void triggerSource(const std::string &sourceName) = 0;
  virtual void start() = 0;
  virtual void stop() = 0;

  auto getNormalPriorityQueue() -> bswi::util::SafeList<std::shared_ptr<IEventSource>> &
  {
    return m_normalPriority;
  }
  auto getHighPriorityQueue() -> bswi::util::SafeList<std::shared_ptr<IEventSource>> &
  {
    return m_highPriority;
  }

public:
  const std::size_t m_max_events = 1024;
  std::size_t m_pollInterval;

protected:
  std::atomic<bool> m_running = false;
  std::string m_name;
  bool m_ownThread;

private:
  bswi::util::SafeList<std::shared_ptr<IEventSource>> m_normalPriority{"normalPriorityQueue"};
  bswi::util::SafeList<std::shared_ptr<IEventSource>> m_highPriority{"highPriorityQueue"};
};

} // namespace bswi::event
